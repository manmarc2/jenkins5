FROM debian:11.6

WORKDIR /usr/src/app
COPY . .

RUN apt-get update -y
RUN apt-get install -y python3 python-is-python3 python3-pip
RUN pip install -r requirements.txt

EXPOSE 5000

CMD ["python3", "app.py"]